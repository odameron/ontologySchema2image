#! /usr/bin/env python3

import graphviz
import networkx as nx
import pathlib
import rdflib
from networkx.drawing.nx_agraph import write_dot
import sys
import time


sparqlPrefixes = """
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX owl: <http://www.w3.org/2002/07/owl#>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX sh: <http://www.w3.org/ns/shacl#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX void: <http://rdfs.org/ns/void#>
"""

def formatURI(classID, uriPrefix):
    if uriPrefix.startswith("http"):
        return "<" + uriPrefix + classID + ">"
    return uriPrefix + ":" + classID

def getLocalName(uri, uriPrefix):
    if uri.startswith("<"):
        return uri[1:-1].replace(uriPrefix, "")
    if uri.startswith("http"):
        return uri.replace(uriPrefix, "")
    if uriPrefix.endswith(":"):
        if uri.startswith(uriPrefix):
            return uri.replace(uriPrefix, "")
    if uri.startswith(uriPrefix + ":"):
        return uri.replace(uriPrefix + ":", "")
    return uri

def createOntologySchemaGraphShacl(rdfGraph, filePath="", displayURIs=False):
    schemaGraph = graphviz.Digraph('G')
    #schemaGraph.attr(rankdir="LR")
    schemaGraph.attr(rankdir="BT")
    #schemaGraph.attr(layout="fdp")
    #schemaGraph.attr(len='0.8')
    #schemaGraph.attr(K='0.8')

    prefixToNamespace = {}
    namespaceToPrefix = {}
    for currentNS in rdfGraph.namespace_manager.namespaces():
        prefixToNamespace[currentNS[0]] = currentNS[1]
        namespaceToPrefix[currentNS[1]] = currentNS[0]


    scriptDir = pathlib.Path(__file__).parent.absolute()
    sparqlQuery = pathlib.Path(scriptDir / 'queries/getClasses.rq').read_text()
    qres = rdfGraph.query(sparqlQuery)
    for row in qres:
        nodeIdent = rdfGraph.namespace_manager.normalizeUri(str(row[0])).replace(":", "-")
        nodeLabel = str(row[1])
        if nodeLabel == "" or (nodeLabel == "None") or (nodeLabel == ""):
            nodeLabel = rdfGraph.namespace_manager.normalizeUri(str(row[0]))
        elif displayURIs:
            nodeLabel += "\n" + rdfGraph.namespace_manager.normalizeUri(str(row[0]))
        schemaGraph.node(nodeIdent, label=nodeLabel, shape='box', color='black', fontcolor='black')
    
    sparqlQuery = pathlib.Path(scriptDir / 'queries/getShaclObjectProperties.rq').read_text()
    qres = rdfGraph.query(sparqlQuery)
    for row in qres:
        propIdent = rdfGraph.namespace_manager.normalizeUri(str(row[0])).replace(":", "-")
        propLabel = str(row[1])
        if (propLabel == None) or (propLabel == "None") or (propLabel == ""):
            propLabel = rdfGraph.namespace_manager.normalizeUri(str(row[0]))
        elif displayURIs:
            propLabel += "\n" + rdfGraph.namespace_manager.normalizeUri(str(row[0]))
        sourceIdent = rdfGraph.namespace_manager.normalizeUri(str(row[2])).replace(":", "-")
        destIdent = rdfGraph.namespace_manager.normalizeUri(str(row[3])).replace(":", "-")
        schemaGraph.edge(sourceIdent, destIdent, label=propLabel)
    
    sparqlQuery = pathlib.Path(scriptDir / 'queries/getShaclDatatypeProperties.rq').read_text()
    qres = rdfGraph.query(sparqlQuery)
    propertySuffix = 0
    for row in qres:
        propIdent = rdfGraph.namespace_manager.normalizeUri(str(row[0])).replace(":", "-")
        propLabel = str(row[1])
        if (propLabel == None) or (propLabel == "None") or (propLabel == ""):
            propLabel = rdfGraph.namespace_manager.normalizeUri(str(row[0]))
        elif displayURIs:
            propLabel += "\n" + rdfGraph.namespace_manager.normalizeUri(str(row[0]))
        sourceIdent = rdfGraph.namespace_manager.normalizeUri(str(row[2])).replace(":", "-")
        destIdent = rdfGraph.namespace_manager.normalizeUri(str(row[3])).replace(":", "-") + str(propertySuffix)
        destLabel = rdfGraph.namespace_manager.normalizeUri(str(row[3])).replace(":", "-")
        schemaGraph.node(destIdent, label=destLabel, shape='box', color='gray', fontcolor='gray', style='rounded')
        schemaGraph.edge(sourceIdent, destIdent, label=propLabel, color='gray', fontcolor='gray')
        propertySuffix += 1

    schemaGraph.save(filename=filePath)


if __name__ == "__main__":
    rdflibFormat = {}
    rdflibFormat['.owl'] = 'xml'
    rdflibFormat['.ttl'] = 'ttl'

    if len(sys.argv) != 2:
        print("Error: Wrong number of arguments")
        print("Usage: " + sys.argv[0] + " [ontologyFilePath]")
        sys.exit(1)
    
    datasetPath = sys.argv[1]
    datasetFormat = rdflibFormat[pathlib.PurePath(datasetPath).suffix]
    schemaFileName = pathlib.PurePath(datasetPath).stem + "-schema.dot"
    schemaPath = pathlib.PurePath(datasetPath).with_name(schemaFileName)

    rdfGraph = rdflib.Graph()
    rdfGraph.parse(datasetPath, format=datasetFormat)


    #createGraphDescendants(rdfGraph, classOfInterest, prefixOfInterest, 'example/' + classOfInterest + '-descendants.dot')
    #createGraphAncestors(rdfGraph, classOfInterest, prefixOfInterest, 'example/' + classOfInterest + '-ancestors.dot')
    createOntologySchemaGraphShacl(rdfGraph, schemaPath)

    print("Generated diagram: " + str(schemaPath))
    print("  convert to png with: dot -Tpng " + str(schemaPath) + " -o " + str(schemaPath)[:-3] + "png")
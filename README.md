# ontologySchema2image

Utility script for generating a graph representation of an ontology's schema (i.e. the classes and the relations).


# Dependencies

- Python3 
- [rdflib](https://github.com/RDFLib/rdflib) 
- [networkx](https://networkx.org/)


# Usage

## Command

```
./ontologySchema2image.py [ontologyFilePath]
```


## Example

For example, the direct visual rendering of the `example/simpleOntologySchema.owl` file is:

![Direct rendering of the triples representing a simple ontology.](example/simpleOntologySchema-orig.png)

Running

```
./ontologySchema2image.py example/simpleOntologySchema.owl
```

generates the file `example/simpleOntologySchema-schema.dot`.
You can then either directly use the dot file (or visualize it with `xdot`), or convert it into an image with
```
dot -Tpng example/simpleOntologySchema-schema.dot -o example/simpleOntologySchema-schema.png
```
(executing the `ontologySchema2image.py` script displays the proper command).

![Rendering of the schema of a simple ontology.](example/simpleOntologySchema-schema.png)


# See also:

- [https://gitlab.com/odameron/rdf2image](rdf2image) for generating a graph representation of a (simple) RDF file
- [https://gitlab.com/odameron/ontologyHierarchyVisualization](ontologyHierarchyVisualization) for generating a graphical representation of the hierarchy of a (fraction of an) ontology


# Todo

- ontology source
    - [ ] local file
    - [ ] SPARQL endpoint
- classes 
    - [x] handle `rdfs:Class` and `owl:Class`
    - [x] handle labels
    - [x] handle `rdfs:subClassOf`
- properties
    - [x] handle object properties
    - [x] handle datatype properties
    - [ ] handle `rdf:property`
        - [ ] check whether both nodes exist
    - [ ] handle labels
        - [x] retrieve labels
        - [ ] display labels if present
    - [ ] handle existential and universal constraints
- [ ] handle ontology version
- [ ] ontology path should be an argument
- [ ] consider VOWL and WebVOWL (http://vowl.visualdataweb.org/webvowl.html)
- currently the `createOntologySchemaGraph(...)` function has an optional argument `displayURIs=False` that must be toggled from the script. Add an option to the command line.
